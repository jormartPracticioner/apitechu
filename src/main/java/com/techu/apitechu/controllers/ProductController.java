package com.techu.apitechu.controllers;
import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseURL ="/apitechu/v1";

    @GetMapping(APIBaseURL +"/products")
    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProducts");
        return ApitechuApplication.productModels;

    }

    @GetMapping(APIBaseURL +"/products/{id}")
    public ProductModel getProductById(@PathVariable String id){

        System.out.println("getProductById");
        System.out.println("el id es "+ id);

        //return new ProductModel("0","Prueba",10f);

        ProductModel result = new ProductModel();

        for(ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id))
                result = product;
        }
        return result;

    }

    @PostMapping(APIBaseURL +"/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){

        System.out.println("createProduct");
        System.out.println("Id del nuevo producto es " + newProduct.getId());
        System.out.println("Desc del nuevo producto es " + newProduct.getDesc());
        System.out.println("Precio del nuevo producto es " + newProduct.getPrice());

        //Lo añadimos al listado para poder obtenerlo después
        ApitechuApplication.productModels.add(newProduct);

        return newProduct;

    }

    @PutMapping(APIBaseURL +"/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("Id del producto a actualizar en URL es " + id);
        System.out.println("Id del producto a actualizar es " + product.getId());
        System.out.println("Desc del producto a actualizar es " + product.getDesc());
        System.out.println("Precio del producto a actualizar es " + product.getPrice());

        ProductModel result = new ProductModel();

        for(ProductModel prod : ApitechuApplication.productModels) {
            if (prod.getId().equals(id))
                result = prod;
        }
        if (result.getId()==null)
        {
            System.out.println("ERROR: producto no encontrado");
        }
        else {
            result.setDesc(product.getDesc());
            result.setPrice(product.getPrice());
            System.out.println("Producto actualizado");
        }

        return result;
    }

    @PatchMapping(APIBaseURL +"/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel product, @PathVariable String id){

        // Casos a probar: No enviar nada / Enviar Desc y Price / Enviar sólo Desc / Enviar sólo Price

        System.out.println("patchProduct");
        System.out.println("Id del producto a patchear en URL es " + id);
        System.out.println("Desc del producto a patchear es " + product.getDesc());
        System.out.println("Precio del producto a patchear es " + product.getPrice());

        ProductModel result = new ProductModel();

        for(ProductModel prod : ApitechuApplication.productModels) {
            if (prod.getId().equals(id))
                result = prod;
        }
        if (result.getId()==null)
        {
            System.out.println("ERROR: producto no encontrado");
        }
        else {
            if(product.getDesc() != null) {
                result.setDesc(product.getDesc());
                System.out.println("Desc actualizado");
            }
            if(product.getPrice() != null) {        //Revisar validacion, probar con precio > 0
                result.setPrice(product.getPrice());
                System.out.println("Price actualizado");
            }
            if(product.getDesc() == null && product.getPrice() == null) {
                System.out.println("Producto no actualizado");
            }

        }
        return result;
    }




    @DeleteMapping(APIBaseURL +"/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id)
    {
        System.out.println("deleteProduct");
        System.out.println("Id del producto a borrar es " + id);

        ProductModel result = new ProductModel();
        Boolean foundProduct = false;

        for(ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                System.out.println("Producto a borrar encontrado");
                result = product;
                foundProduct = true;
            }
        }

        if(foundProduct == true){
            System.out.println("Producto borrado");
            ApitechuApplication.productModels.remove(result);
        }
        else
        {
            System.out.println("Producto no encontrado");
        }
        return new ProductModel();
    }

}
