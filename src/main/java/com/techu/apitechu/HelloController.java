package com.techu.apitechu;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "Hola index Controller";
    }

    @RequestMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "TechU") String name){
        return "Hola Controller: "+name;
    }

    //Ejemplo llamada: http://localhost:8080/sum?val1=70&val2=50
    @RequestMapping("/sum")
    public String sum(@RequestParam(value = "val1", defaultValue = "0") int val1, @RequestParam(value = "val2", defaultValue = "0") int val2){
        return "Hola Sum: "+val1+" + "+val2+ " = "+(val1+val2);
    }
}
