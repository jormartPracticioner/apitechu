package com.techu.apitechu.models;

public class ProductModel {
    private String id;
    private String desc;
    private Float price;

    public ProductModel(){

    }

    public ProductModel(String id, String desc, Float price) {
        this.id = id;
        this.desc = desc;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }

    public Float getPrice() {
        return price;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
